import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class fabricaDesafio {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver", "C:/Users/vinif/Downloads/geckodriverl-v0.30.0-win64/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void formulario() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[1]/td/input")).sendKeys("walece");
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[2]/td/input")).sendKeys("professor");
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[3]")).click();
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[5]/td/input[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[6]/td/input[2]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[7]/td/select/option[3]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div/div[3]/form/table/tbody/tr[9]/td/input[2]")).click(); //submit
        Thread.sleep(3000);
        Assert.assertEquals("Processed Form Details", driver.findElement(By.xpath("/html/body/div/h1")).getText());
        Thread.sleep(3000);


    }
}
